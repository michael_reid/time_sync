

#ifndef TIMESYNC_TIME_SYNCHRONIZER_H
#define TIMESYNC_TIME_SYNCHRONIZER_H


#include <ostream>

class time_synchronizer {
public:
    time_synchronizer()= default;
    virtual ~time_synchronizer() = default;

    friend std::ostream &operator<<(std::ostream &os, const time_synchronizer &synchronizer);

private:

};


#endif //TIMESYNC_TIME_SYNCHRONIZER_H
